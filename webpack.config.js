module.exports = {
  devtool: "inline-source-map",
  entry: __dirname + "/app.jsx",
  output: {
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        use: [
         "style-loader",
         "css-loader",
        ]
      }
    ],
    loaders: [
      {
        test: /\.jsx?/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
    ]
  }
};
           